#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int power(int base, int exponent){
    int result=1;
    for (exponent; exponent>0; exponent--){
        result = result * base;
    }
    return result;
}
//struct para simular as informações de um processo.
typedef struct processos{
    int index;
    int PID;
    int chegadaNaFila;
    int total;
    int totalRodado;
    int cortado;
    int IO[3];
    int nIO; //tipo do IO
    int tIO; //tempo de execução rodado do IO
    int tempoParaIO[3];
}processos;

//struct para se criar as filas ordenadas.
typedef struct filaProcessos{
    processos *atual;
    struct filaProcessos *proximo;
}filaProcessos;

filaProcessos *ptrFilaIoD; 
filaProcessos *ptrFilaIoF;
filaProcessos *ptrFilaIoI;
filaProcessos *ptrTerminados;
filaProcessos *ptrFilaAlta;
filaProcessos *ptrFilaBaixa;

//tentativa de criação da lista de alta prioridade a partir dos valores de chegada do programa
void escalonador(processos *ptrLista, filaProcessos *ptrFilaA, int ut, int qnt, int *k){
    filaProcessos *aux, *aux2 = ptrFilaA;
    //percorre a lista de pseudo-procesos
    for(int i=0; i<qnt; i++){
        if(ptrLista[i].chegadaNaFila == ut){
            *k=*k+1;
            aux = malloc(sizeof(filaProcessos));
            aux->atual = &ptrLista[i];
            aux->proximo = 0;
            while(aux2->proximo){
                aux2 = aux2->proximo;
            }
            if(aux2->atual == 0){
                aux2->atual= aux->atual;
                }//se primeiro da lista coloca no lugar certo
            else{
                aux2->proximo = aux;
                }//os outros no lugar certo
            //getchar();//pausar qndo chega aqui para checagem
            //printf("%d, %d",aux2->atual.index, aux2->proximo->atual->index);
        }
    }
}

void avancaFila(int tipoIO){
    switch (tipoIO){
        case 0:
            if (ptrFilaIoD->proximo) ptrFilaIoD = ptrFilaIoD->proximo; // olha pro proximo
            else ptrFilaIoD = malloc(sizeof(filaProcessos));
            break;
        case 1:
            if (ptrFilaIoF->proximo) ptrFilaIoF = ptrFilaIoF->proximo;
            else ptrFilaIoF = malloc(sizeof(filaProcessos));
            break;
        case 2:
            if (ptrFilaIoI->proximo) ptrFilaIoI = ptrFilaIoI->proximo;
            else ptrFilaIoI = malloc(sizeof(filaProcessos));
            break;
    }
}

void insereNoFimIO(processos* proc, int tipoIO){
    filaProcessos *aux;
    proc->nIO = tipoIO;
    filaProcessos *filaProc = malloc(sizeof(filaProcessos));
    filaProc->atual = proc;
    switch (tipoIO){
        case 0:
            aux = ptrFilaIoD;
            while(aux->proximo){
                aux = aux->proximo;// percorre até o fim da lista e adiciona sempre como proximo
            }
            aux->proximo = filaProc;
            break;
        case 1:
            aux = ptrFilaIoF;
            while(aux->proximo){
                aux = aux->proximo;// percorre até o fim da lista
            }
            aux->proximo = filaProc;
            break;
        case 2:
            aux = ptrFilaIoI;
            while(aux->proximo){
                aux = aux->proximo;// percorre até o fim da lista
            }
            aux->proximo = filaProc;
            break;
    }
}

void insereNoFimProc(int tipoIO, processos * atual){
    filaProcessos *aux;
    filaProcessos *filaProc = malloc(sizeof(filaProcessos));
    filaProc->atual = atual;
    if (tipoIO==0){
        aux = ptrFilaBaixa;
        if (!aux->atual) aux->atual = atual;
        else{ 
            while(aux->proximo){
                aux = aux->proximo;// percorre até o fim da lista
            }
            aux->proximo = filaProc;
        }
    }
    else {
        aux = ptrFilaAlta;
        if (!aux->atual) aux->atual = atual;
        else{ 
            while(aux->proximo){
                aux = aux->proximo;// percorre até o fim da lista
            }
            aux->proximo = filaProc;
        }
    }
}

void insereNoFimBaixa(processos *atual){
    filaProcessos *aux, *aux2;
    filaProcessos *filaProc = malloc(sizeof(filaProcessos));
    filaProc->atual = atual;
    aux = ptrFilaBaixa;
    if (!aux->atual) aux->atual = atual;
    else{ 
        while(aux->proximo){
            aux = aux->proximo;// percorre até o fim da lista
        }
        aux->proximo = filaProc;
    }
    if(!ptrFilaAlta->proximo) ptrFilaAlta->atual= 0;
    else ptrFilaAlta=ptrFilaAlta->proximo;
}

void insereNoFimTerminados(processos * atual){
    filaProcessos *aux, *aux2 = ptrTerminados;
    aux = malloc(sizeof(filaProcessos));
    aux->atual = atual;
    while(aux2->proximo){
        aux2 = aux2->proximo;
    }
    if(!aux2->atual){
        aux2->atual= atual;
    }//se primeiro da lista coloca no lugar certo
    else{
        aux2->proximo = aux;
    }
}

void passarProximoAlta(){
    if(ptrFilaAlta->proximo == 0){
        ptrFilaAlta->atual = 0;
    }
    else ptrFilaAlta = ptrFilaAlta->proximo;
}

void passarProximoBaixa(){
    if(ptrFilaBaixa->proximo == 0){
        ptrFilaBaixa->atual = 0;
    }
    else ptrFilaBaixa = ptrFilaBaixa->proximo;
}

void passarProximoTerminados(){
    if(ptrTerminados->proximo == 0){
        ptrTerminados->atual = 0;
    }
    else ptrTerminados = ptrTerminados->proximo;
}

void passarProximoIoD(){
    if(ptrFilaIoD->proximo == 0){
        ptrFilaIoD->atual = 0;
    }
    else ptrFilaIoD = ptrFilaIoD->proximo;
}

void passarProximoIoF(){
    if(ptrFilaIoF->proximo == 0){
        ptrFilaIoF->atual = 0;
    }
    else ptrFilaIoF = ptrFilaIoF->proximo;
}

void passarProximoIoI(){
    if(ptrFilaIoI->proximo == 0){
        ptrFilaIoI->atual = 0;
    }
    else ptrFilaIoI = ptrFilaIoI->proximo;
}

void gerenciadorFilas(filaProcessos *ptrFilaA, filaProcessos *ptrFilaB, int tamanhoCiclo, int *quantidadeTerminados){
    //Gerencia Processos
    int a=0,b=0;
    processos *current_process;
    if (ptrFilaA->atual){
        printf("peguei da A\n");
        a++;
        current_process= ptrFilaA->atual;//avalia apenas o processo em execução na fila de Alta
        //printf("%d\n", current_process->index);
    }
    else if(ptrFilaB->atual){
        printf("peguei da B\n");
        b++;
        current_process = ptrFilaB->atual;// avalia apenas o processo de execução na fila de Baixa
        //printf("%d\n", current_process->index);
    }
    //verifica se há algum processo
    if (current_process->index!=0){
        current_process->totalRodado += 1; // um instante a mais de execucação
        if (current_process->totalRodado%(tamanhoCiclo+1) != 0){
            printf("entrei na execução do %d, com %d\n", current_process->index, current_process->totalRodado);
            if(current_process->totalRodado <= current_process->total){
                //Chamada para IO
                if(current_process->totalRodado>1 && current_process->totalRodado<current_process->total-3){
                    for (int try=0; try<(power(2,(current_process->totalRodado-2))); try++){
                        //printf("entrei na execução IO do %d\n", current_process->index);
                        int rn_IO = rand()%4;//Randomiza uma das IO para ser chamada
                        if (rn_IO == 3) continue; 
                        if (current_process->tempoParaIO[rn_IO] > 0){
                            printf("entrei na execução do %d, com IO %d\n", current_process->index, rn_IO);
                            insereNoFimIO(current_process,rn_IO); // chama o escalanador de IO
                            if(a) passarProximoAlta();
                            else passarProximoBaixa(); // passa para o proximo processo da fila 
                            // current_process->tempoParaIO[rn_IO] = 0; //Zera o tempo para não haver chamada repetida de IO
                            break;
                        }
                    }
                }
            }
            else{
                insereNoFimTerminados(current_process);
                if(a) passarProximoAlta();
                else passarProximoBaixa();
                printf("entrei Inseri no Terminados\n");
            }
        }
        else {
            current_process->cortado +=1;
            current_process->total += 1;
            insereNoFimBaixa(current_process);
            printf("Cheguei para mudar para a lista Baixa o %d!\n", current_process->index);
        }
    }
    else{
        printf("peguei ngm\n");
    }
    processos *current_io;
    //Gerencia IO Disco
    current_io = ptrFilaIoD->atual;//avalia apenas o processo em execução na fila de Alta
    if (current_io){
        if(current_io->tIO < current_io->tempoParaIO[current_io->nIO]){
            //Chamada para IO
            current_io->tIO += 1; // um instante a mais de execucação
            printf("entrei no IO do %d que é %d, %d tempo passado\n",current_io->index, current_io->nIO, current_io->tIO);
        }
        else{
            avancaFila(current_io->nIO);
            insereNoFimProc(current_io->nIO, current_io);
        }
    }
    else if (ptrFilaIoD->proximo){
        ptrFilaIoD = ptrFilaIoD->proximo;
    }

    //Gerencia IO Fita
    current_io = ptrFilaIoF->atual;//avalia apenas o processo em execução na fila de Alta
    if (current_io){
        if(current_io->tIO < current_io->tempoParaIO[current_io->nIO]){
            //Chamada para IO
            current_io->tIO += 1; // um instante a mais de execucação
            printf("entrei no IO do %d que é %d, %d tempo passado\n",current_io->index, current_io->nIO, current_io->tIO);
        }
        else{
            avancaFila(current_io->nIO);
            insereNoFimProc(current_io->nIO, current_io);
        }
    }
    else if (ptrFilaIoF->proximo){
        ptrFilaIoF = ptrFilaIoF->proximo;
    }

    //Gerencia IO Impressora
    current_io = ptrFilaIoI->atual;//avalia apenas o processo em execução na fila de Alta
    if (current_io){
        if(current_io->tIO < current_io->tempoParaIO[current_io->nIO]){
            //Chamada para IO
            current_io->tIO += 1; // um instante a mais de execucação
            printf("entrei no IO do %d que é %d, %d tempo passado\n",current_io->index, current_io->nIO, current_io->tIO);
        }
        else{
            avancaFila(current_io->nIO);
            insereNoFimProc(current_io->nIO, current_io);
        }
    }
    else if (ptrFilaIoI->proximo){
        ptrFilaIoI = ptrFilaIoI->proximo;
    }
    
    return;
}

void main( int argc, char *argv[]){

    srand(time(NULL));
    int qntPro = atoi(argv[1]);
    //iniciação dos parametros
    processos *ptrInit;
    filaProcessos *auxiliar;
    
    int temposIO[3] = {3, 8, 12};

    ptrInit = malloc(qntPro*sizeof(processos));
    ptrFilaIoD = malloc(sizeof(filaProcessos));
    ptrFilaIoF = malloc(sizeof(filaProcessos));
    ptrFilaIoI = malloc(sizeof(filaProcessos));
    ptrTerminados = malloc(sizeof(filaProcessos));
    ptrFilaAlta = malloc(sizeof(filaProcessos));
    ptrFilaBaixa = malloc(sizeof(filaProcessos));

    ptrFilaIoD->proximo=0;
    ptrFilaIoF->proximo=0;
    ptrFilaIoI->proximo=0;
    ptrTerminados->proximo=0;
    ptrFilaAlta->proximo=0;
    ptrFilaBaixa->proximo=0;

    if(!ptrInit || !ptrFilaAlta || !ptrFilaBaixa || !ptrFilaIoD || !ptrFilaIoF || !ptrFilaIoI || !ptrTerminados){
        printf("Erro de inicialização dos ponteiros");
        return;
        //check do malloc
    }
    //criação dos processos com coisas aleatorias
    for(int i=0;i<qntPro;i++){
        ptrInit[i].index = i+1;
        ptrInit[i].PID = &ptrInit[i];
        ptrInit[i].chegadaNaFila = 0;
        ptrInit[i].cortado = 0;
        ptrInit[i].nIO = 0;
        ptrInit[i].tIO = 0;
        while(!ptrInit[i].chegadaNaFila){
            ptrInit[i].chegadaNaFila = rand()%(2*qntPro+1);
            for(int j=0;j<i;j++){
                if(ptrInit[j].chegadaNaFila == ptrInit[i].chegadaNaFila){ 
                    ptrInit[i].chegadaNaFila = 0;
                    break;
                }
            }
        }
        ptrInit[i].total = 0;
        while(!ptrInit[i].total){
            ptrInit[i].total = rand()%17;
            if(ptrInit[i].total < 5) ptrInit[i].total+=5;
        }
        ptrInit[i].totalRodado = 0;
        for(int j=0;j<3;j++){
            ptrInit[i].IO[j] = rand()%2; //randomiza 0 ou 1 para se vai chamar a IO ou não
            ptrInit[i].tempoParaIO[j] = ptrInit[i].IO[j]* temposIO[j]; //tempo para cada tipo de IO
        }
    }
    //print dos processos criados
    printf("Lista de Processos:\n");
    for (int i = 0; i < atoi(argv[1]); i++)
    {
        printf("Processo %d:\n", i+1);
        printf("\tPID: %x Ox\n", ptrInit[i].PID);
        printf("\tChegada: %d\n", ptrInit[i].chegadaNaFila);
        printf("\tTotal a ser rodado: %d u.t\n", ptrInit[i].total);
        printf("\tTotal Rodado: %d u.t\n", ptrInit[i].totalRodado);
        printf("\tIO necessarios e seus tempos:\n");
        for(int j=0;j<3;j++){
            if(ptrInit[i].IO[j]&&j==0) printf("\t\tDisco e %d u.t\n", ptrInit[i].tempoParaIO[j]);
            if(ptrInit[i].IO[j]&&j==1) printf("\t\tFita e %d u.t\n", ptrInit[i].tempoParaIO[j]);
            if(ptrInit[i].IO[j]&&j==2) printf("\t\tImpressora e %d u.t", ptrInit[i].tempoParaIO[j]);
        }
    printf("\n");
    printf("\n");   
    }
    getchar();
    //teste da criação da lista.
    int contagem = 0, terminados=0;
    for(int i = 0; i<300; i++){
        printf("turno %d\n", i);
        escalonador(ptrInit, ptrFilaAlta, i, qntPro, &contagem);
        gerenciadorFilas(ptrFilaAlta,ptrFilaBaixa, 4, &terminados);
        getchar();
        }
}

